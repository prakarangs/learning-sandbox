# Set up project with Gulp and Bower
### Set up Git Repo
Build your basic project structure in Repo

### Bower package manager
Bower is a quick package manager for front-end

1. Go to project root directory ```cd ~/projects/my-project```

2. Create bower.json or ``` bower init ```
	* add this in the file
	``` {
  "name": "my-project"
} ```

3. Create .bowerrc to set path

4. When pushing commit to repo, always delete bower components file but leave the json files : ``` .bowerrc ``` and ```bower.json```

5. Whenever you pull down your code you can reinstall everything by running this command

``` bower install ```

### Bower command
1. Search package ``` bower search <query> ```
	* ex. bower search jquery

2. Bower install <package> --save
	* --save will add to bower.json
	* you can specify version by #

	example. ```bower install jquery#9.2 --save```

3. ``` bower update ``` for updating packages

4. ``` bower uninstall <package> --save ```

### Directory Setting
By default Bower is going to download everything to the bower_components folder. We can change this using the directory setting in the .bowerrc file. If this file doesn’t yet exist you will need to add it to the same folder that contains your bower.json file. This file is a JSON file for defining your Bower settings. To change the folder that Bower installs packages add the directory setting like:-

``` {
  "directory": "public/vendor"
}
```

This will cause Bower to download everything to public/vendor/ instead of bower_components.

### Add Gulp to Project
1. Go to project root directory ```cd ~/projects/my-project```
2. Create "package.json" file
	* The project needs this file for dependencies (also you can do ```npm init```)
	* If you do it manually put this in the file
	```
	{
  "name": "my-project",
  "version": "0.1.0",
  "devDependencies": {
  }
}```
	

3. Install Gulp locally ```npm install gulp --save-dev```
	
	* The ```--save-dev``` flag will automatically add Gulp to the depenencies of your package.json file.
	* You should also see a new folder in your project called ```node_modules``` (check using ls or dir if you’re using Windows).

4. Create gulpfile.js (this is like the instruction file)

	* require gulp in your gulpfile

	``` var gulp = require('gulp'); ```

### 4 Gulp methods

* **gulp.task(name, fn)** – registers a function with a name
* **gulp.watch(glob, fn)** – runs a function when a file that matches the glob changes
* **gulp.src(glob)** – returns a readable stream
* **gulp.dest(folder)** – returns a writable stream
* **glob** is usually path of file

### Plugins on Gulp

Gulp on its own does not do a lot. We have to install plugins and create task for gulp.
* concat
* jshint
* notify
* compass
* uglify
* rename

To use pluging there are certain step required:

1. Install plugin via ```npm install <plugin> --save-dev ```
	You should see the plugin dependencies in your package.json

2. Add task to your gulpfile:
	```var concat = require('gulp-concat');
 // Concatenate JS Files
gulp.task('scripts', function() {
    return gulp.src('src/js/*.js')
      .pipe(concat('main.js'))
      .pipe(gulp.dest('build/js'));
});
 // Default Task
gulp.task('default', ['scripts']);```

* First require the plugin.
* create certain task using the gulp methods.
* set gulp default task


