$(document).ready(function() {
 	console.log('test');
 	var     owl = $("#demo");
  owl.owlCarousel({
  	autoPlay: false, //Set AutoPlay to 3 seconds
      items : 1,
      singleItem: true,
      pagination: false,
      navigation:true,
	  navigationText: [
      "<i class='icon-chevron-left icon-white'><</i>",
      "<i class='icon-chevron-right icon-white'>></i>"
      ]
  });


	// clickToggle function
	(function($) {
    $.fn.clickToggle = function(func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function() {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
	}(jQuery));

	// Animate items
	$('#btn').clickToggle(function() {
	    //move to left
		$('#left').animate({
			left: "-150"
		}, 2000);
		// move to right
		$('#right').animate({
			right: "-150"
		}, 2000);
		// slideUp VDO
		$('#vdo').animate({
			opacity: "1",
			top: "100"
		}, 2000);

	}, function() {
	    //move to left
		$('#left').animate({
			left: "0"
		}, 2000);
		// move to right
		$('#right').animate({
			right: "0"
		}, 2000);
		// hide VDO
		$('#vdo').animate({
			opacity: "0",
			top: "300"
		}, 2000);
	});


});