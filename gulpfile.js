// ----------------------------------------------------------------
// Gulpfile configuration

// Include Gulp
var gulp = require('gulp');

// Include Plugin
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

// ----------------------------------------------------------------
// Minify JS
gulp.task('scripts', function() {
	return gulp.src('./vendor/jquery/dist/jquery.js')
	.pipe(rename({ 
		suffix: '.min'}))
	.pipe(uglify())
	.pipe(gulp.dest('./htdocs/js'));
});

// Pagu structure
gulp.task('pagu', function() {
	return gulp.src('./vendor/pagu-frontend/sass/**/*.scss')
		.pipe(gulp.dest('.htdocs/sass'));
});

// Watch tasks
gulp.task('watch', function() {
	// watch something
});

// Default Task
gulp.task('default', ['scripts', 'pagu']);

